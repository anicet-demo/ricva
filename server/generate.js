var faker = require('faker')

var databaseEmp = { empotages: [] };
for (let i = 0; i <= 5; i++) {
  databaseEmp.empotages.push({
    id: i,
    lot: faker.datatype.number(0, 2),
    charge: faker.datatype.number(0, 8),
    create: faker.date.past(),
    update: faker.date.past(),
    numTicket: faker.datatype.number(0, 3),
    numLot: faker.datatype.number(0, 3),
    etat: true
  })
}

var databaseEnt = { entrepots: [] };
for (let i = 0; i <= 3; i++) {
  databaseEnt.entrepots.push({
    id: i,
    libelle: faker.lorem.word(8),
    create: faker.date.past(),
    update: faker.date.past()
  })
}

var databaseCont = { conteneurs: [] };
for (let i = 0; i <= 3; i++) {
  databaseCont.conteneurs.push({
    id: i,
    libelle: 'conteneur '+ i,
    create: faker.date.past(),
    update: faker.date.past()
  })
}

var databaseBook = { bookings: [] };
for (let i = 0; i <= 3; i++) {
  databaseBook.bookings.push({
    id: i,
    libelle: 'booking '+ i,
    create: faker.date.past(),
    update: faker.date.past()
  })
}

var databaseTran = { transitaires: [] };
for (let i = 0; i <= 3; i++) {
  databaseTran.transitaires.push({
    id: i,
    libelle: 'transitaire '+ i,
    create: faker.date.past(),
    update: faker.date.past()
  })
}

var databaseLot = { lots: [] };
for (let i = 0; i <= 9; i++) {
  databaseLot.lots.push({
    id: i,
    qte: faker.datatype.number(0, 4),
    th: faker.datatype.number(0, 4),
    grainage: faker.datatype.number(0, 4),
    sackStock: faker.datatype.number(0, 4),
    sackRestant: faker.datatype.number(0, 4),
    libelle: 'lot '+ i,
    create: faker.date.past(),
    update: faker.date.past()
  })
}

console.log(JSON.stringify(databaseEmp), JSON.stringify(databaseEnt), JSON.stringify(databaseCont),
            JSON.stringify(databaseBook), JSON.stringify(databaseTran), JSON.stringify(databaseLot));
