import { Booking } from "./booking";
import { Conteneur } from "./conteneur";
import { Contrat } from "./contrat";
import { Entrepot } from "./entrepot";
import { Transitaire } from "./transitaire";

export interface Empotage {
  uuid?: any;
  id?: any;
  premium?: number;
  quantite?: number;
  discompte?: number;
  charge?: any;
  numero?: any;
  lot?: any;
  numLot?: any;
  etat?: any;
  create?: any;
  update?: any;
  numTicket?: any;
  contrat?: Contrat;
  conteneur?: Conteneur;
  booking?: Booking;
  transitaire?: Transitaire;
  entrepot?: Entrepot;
}
