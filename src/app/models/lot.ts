export interface Lot {
  uuid?: any;
  id?: any;
  qte?: number;
  th?: number;
  sacStock: number;
  sacRestant?: number;
  grainage?: number;
  libelle?: string;
  create?: any;
  update?: any;
}
