import { Conteneur } from "./conteneur";
import { Empotage } from "./empotage";

export interface Execute {
  uuid?: any;
  id?: any;
  empotage?: Empotage;
  conteneurs?: Conteneur[];
  create?: any;
  update?: any;
}
