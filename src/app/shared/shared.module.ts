import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import { DataTableComponent } from './data-table/data-table.component';
import { DetailBlockComponent } from './detail-block/detail-block.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [
    LoadingComponent,
    DataTableComponent,
    DetailBlockComponent
  ],
  declarations: [
    LoadingComponent,
    DataTableComponent,
    DetailBlockComponent
  ],
})
export class SharedModule { }
