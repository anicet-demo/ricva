import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-detail-block',
  templateUrl: './detail-block.component.html',
  styleUrls: ['./detail-block.component.scss']
})
export class DetailBlockComponent implements OnInit {

  @Input() datas: any 
  @Input() etat: boolean = false
  @Input() entity: string = ""

  form: FormGroup
  constructor(
    private formBuild: FormBuilder
  ) {
    this.newForm()
  }

  ngOnInit(): void {
  }
  newForm(){
    this.form = this.formBuild.group({
      id: [null],
      contrat: [null],
      qte: [0],
      premium: [0],
      discompte: [0],
      update: [null],
    })
  }
  action(action){
    if (action === 'editer') {

    }
    if (action === 'enregistrer') {
      this.onSubmit()

    }
    if (action === 'annuler') {

    }
  }
  onSubmit(){}
}
