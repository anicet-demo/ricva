import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpotageService } from 'src/app/services/empotage.service';
import { ExecuteService } from 'src/app/services/execute.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() datas: any = []
  @Input() entity: string = ""
  @Input() title: string = ""

  executeRow = [
    { label: "N° Exécution", value: "N° Exécution" },
    { label: "Empotage", value: "Empotage" },
    { label: "Date de création", value: "Date de création" },
    { label: "Modifier le", value: "Modifier le" },
    { label: "Status", value: "Status" },
    { label: "Actions", value: "Actions" }
  ]
  empotageRow = [
    { label: "N° Lot", value: "N° Lot" },
    { label: "Chargement", value: "Chargement" },
    { label: "Date de création", value: "Date de création" },
    { label: "Modifier le", value: "Modifier le" },
    { label: "Numero de ticket de pesée", value: "Numero de ticket de pesée" },
    { label: "Status", value: "Status" },
    { label: "Actions", value: "Actions" }
  ]
  thead = []
  body = []
  last = 0
  constructor(
    public router: Router,
    private executeService: ExecuteService,
    private empotageService: EmpotageService
  ) { }

  ngOnInit(): void {
    if (this.entity === "empotage") {
      this.thead = this.empotageRow
    } else if (this.entity === "execution") {
      this.thead = this.executeRow
    }
    this.last = this.thead.length -1
  }
  show(item){
    this.router.navigate(['/pages/'+this.entity+'/single/'+item.id])
  }
  delete(item, i){
    Swal.fire({
      title:  "<h5 style='color:#CB7A4B; font-size: 2rem; font-weight: bold; margin-top:30px'>" + 'Voulez-vous supprimer' + "</h5>",
      text: " Vous ne pourrez pas revenir en arrière !",
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonColor: '#f5d6c8',
      cancelButtonColor: '#CB7A4B',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then((result) => {
      if (result.isConfirmed) {
        if (this.entity === "empotage") {
          this.empotageService.getDelete(item.id).subscribe(res => {
            this.datas.splice(0, i)
          }, error => {})
        } else if (this.entity === "execution") {
          this.executeService.getDelete(item.id).subscribe(res => {
            this.datas.splice(0, i)
          }, error => {})
        }
        Swal.fire({
          title: 'Supprimer!',
          text: '',
          icon: 'success',
          confirmButtonColor: '#f5d6c8',
          confirmButtonText: 'Oui',
        })
      }
    })

  }

}
