import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './layouts/components/dashboard/dashboard.component';
import { DefaultComponent } from './layouts/default/default.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: '',
    component: DefaultComponent,
    
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'pages',
        loadChildren: () => import('./pages/pages.module').then(module => module.PagesModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
// import { HomeComponent } from './layout/home/home.component';
// import { DashboardComponent } from './layouts/components/dashboard/dashboard.component';

// const routes: Routes = [
//   { path: '', redirectTo: 'home', pathMatch: 'full' },
//   { path: 'home', component: HomeComponent },
//   { path: 'dashboard', component: DashboardComponent },
  
//   { path: 'pages',
//     loadChildren: () => import('./pages/pages.module').then(module => module.PagesModule)
//   },
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }

