import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Conteneur } from '../models/conteneur';
import { ApiService } from '../utils/api.service';

@Injectable({
  providedIn: 'root'
})
export class ConteneurService implements Resolve<Conteneur[]> {
  conteneur: Conteneur;
  public edit: boolean = false;
  public type: string = "";
  private url = "conteneurs";

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Conteneur[]> | Promise<Conteneur[]> | Conteneur[] {
    return this.getList();
  }
  setLot(conteneur: Conteneur) {
    this.conteneur = conteneur
  }

  getLot(): Conteneur {
    return this.conteneur
  }

  add(data: Conteneur): Observable<any> {
    if (data.uuid) {
      return this.update(data);
    } else {
      return this.create(data);
    }
  }

  create(data: Conteneur): Observable<any> {
    return this.api._post(`${this.url}/new`, data).pipe(
      map((response: any) => response.data),
      catchError((error: any) => throwError(error))
    );
  }

  update(data: Conteneur): Observable<any> {
    return this.api._post(`${this.url}/${data.uuid}/edit`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getList(): Observable<Conteneur[]> {
    return this.api._get(`${this.url}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }


  getSingle(id: string): Observable<Conteneur> {
    return this.api._get(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getDelete(uuid: string): Observable<any> {
    return this.api._delete(`${this.url}/${uuid}/delete`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
