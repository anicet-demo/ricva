import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Execute } from '../models/execute';
import { ApiService } from '../utils/api.service';

@Injectable({
  providedIn: 'root'
})
export class ExecuteService implements Resolve<Execute[]>{
  execute: Execute;
  public edit: boolean = false;
  public type: string = "";
  private url = "executes";

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Execute[]> | Promise<Execute[]> | Execute[] {
    return this.getList();
  }

  setExecute(execute: Execute) {
    this.execute = execute
  }

  getExecute(): Execute {
    return this.execute
  }

  add(data: Execute): Observable<any> {
    if (data?.id) {
      return this.update(data);
    } else {
      return this.create(data);
    }
  }

  create(data: Execute): Observable<any> {
    return this.api._post(`${this.url}`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  update(data: Execute): Observable<any> {
    return this.api._post(`${this.url}/${data.id}/edit`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getList(): Observable<Execute[]> {
    return this.api._get(`${this.url}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }


  getSingle(id: string): Observable<Execute> {
    return this.api._get(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getDelete(id: string): Observable<any> {
    return this.api._delete(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
