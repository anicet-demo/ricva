import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Entrepot } from '../models/entrepot';
import { ApiService } from '../utils/api.service';

@Injectable({
  providedIn: 'root'
})
export class EntrepotService implements Resolve<Entrepot[]> {
  entrepot: Entrepot;
  public edit: boolean = false;
  public type: string = "";
  private url = "entrepots";

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Entrepot[]> | Promise<Entrepot[]> | Entrepot[] {
    return this.getList();
  }

  setLot(entrepot: Entrepot) {
    this.entrepot = entrepot
  }

  getLot(): Entrepot {
    return this.entrepot
  }

  add(data: Entrepot): Observable<any> {
    if (data.uuid) {
      return this.update(data);
    } else {
      return this.create(data);
    }
  }

  create(data: Entrepot): Observable<any> {
    return this.api._post(`${this.url}/new`, data).pipe(
      map((response: any) => response.data),
      catchError((error: any) => throwError(error))
    );
  }

  update(data: Entrepot): Observable<any> {
    return this.api._post(`${this.url}/${data.uuid}/edit`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getList(): Observable<Entrepot[]> {
    return this.api._get(`${this.url}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }


  getSingle(id: string): Observable<Entrepot> {
    return this.api._get(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getDelete(uuid: string): Observable<any> {
    return this.api._delete(`${this.url}/${uuid}/delete`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
