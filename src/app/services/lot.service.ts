import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Lot } from '../models/lot';
import { ApiService } from '../utils/api.service';

@Injectable({
  providedIn: 'root'
})
export class LotService implements Resolve<Lot[]>{
  lot: Lot;
  public edit: boolean = false;
  public type: string = "";
  private url = "lots";

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Lot[]> | Promise<Lot[]> | Lot[] {
    return this.getList();
  }

  setLot(lot: Lot) {
    this.lot = lot
  }

  getLot(): Lot {
    return this.lot
  }

  add(data: Lot): Observable<any> {
    if (data.uuid) {
      return this.update(data);
    } else {
      return this.create(data);
    }
  }

  create(data: Lot): Observable<any> {
    return this.api._post(`${this.url}/new`, data).pipe(
      map((response: any) => response.data),
      catchError((error: any) => throwError(error))
    );
  }

  update(data: Lot): Observable<any> {
    return this.api._post(`${this.url}/${data.uuid}/edit`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getList(): Observable<Lot[]> {
    return this.api._get(`${this.url}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }


  getSingle(id: string): Observable<Lot> {
    return this.api._get(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getDelete(uuid: string): Observable<any> {
    return this.api._delete(`${this.url}/${uuid}/delete`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
