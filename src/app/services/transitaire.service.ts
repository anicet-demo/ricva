import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Transitaire } from '../models/transitaire';
import { ApiService } from '../utils/api.service';

@Injectable({
  providedIn: 'root'
})
export class TransitaireService implements Resolve<Transitaire[]> {
  transitaire: Transitaire;
  public edit: boolean = false;
  public type: string = "";
  private url = "transitaires";

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Transitaire[]> | Promise<Transitaire[]> | Transitaire[] {
    return this.getList();
  }

  setLot(transitaire: Transitaire) {
    this.transitaire = transitaire
  }

  getLot(): Transitaire {
    return this.transitaire
  }

  add(data: Transitaire): Observable<any> {
    if (data.uuid) {
      return this.update(data);
    } else {
      return this.create(data);
    }
  }

  create(data: Transitaire): Observable<any> {
    return this.api._post(`${this.url}/new`, data).pipe(
      map((response: any) => response.data),
      catchError((error: any) => throwError(error))
    );
  }

  update(data: Transitaire): Observable<any> {
    return this.api._post(`${this.url}/${data.uuid}/edit`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getList(): Observable<Transitaire[]> {
    return this.api._get(`${this.url}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }


  getSingle(id: string): Observable<Transitaire> {
    return this.api._get(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getDelete(uuid: string): Observable<any> {
    return this.api._delete(`${this.url}/${uuid}/delete`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
