import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { Empotage } from '../models/empotage';
import { ApiService } from '../utils/api.service';

@Injectable({
  providedIn: 'root'
})
export class EmpotageService  implements Resolve<Empotage[]>{
  empotage: Empotage;
  public edit: boolean = false;
  public type: string = "";
  private url = "empotages";

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Empotage[]> | Promise<Empotage[]> | Empotage[] {
    return this.getList();
  }

  setLot(empotage: Empotage) {
    this.empotage = empotage
  }

  getLot(): Empotage {
    return this.empotage
  }

  add(data: Empotage): Observable<any> {
    if (data?.id) {
      return this.update(data);
    } else {
      return this.create(data);
    }
  }

  create(data: Empotage): Observable<any> {
    return this.api._post(`${this.url}`, data).pipe(
      map((response: any) => response.data),
      catchError((error: any) => throwError(error))
    );
  }

  update(data: Empotage): Observable<any> {
    return this.api._post(`${this.url}/${data.uuid}/edit`, data).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getList(): Observable<Empotage[]> {
    return this.api._get(`${this.url}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }


  getSingle(id: string): Observable<Empotage> {
    return this.api._get(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }

  getDelete(id: string): Observable<any> {
    return this.api._delete(`${this.url}/${id}`).pipe(
      map((response: any) => response),
      catchError((error: any) => throwError(error))
    );
  }
}
