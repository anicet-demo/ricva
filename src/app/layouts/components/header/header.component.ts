import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  selectedCountryCode = 'fr';
  countryCodes = ['fr', 'us'];

  changeSelectedCountryCode(value: string): void {
    this.selectedCountryCode = value;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
