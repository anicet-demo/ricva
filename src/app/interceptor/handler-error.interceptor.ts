import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {EmitterService} from '../services/emitter.service';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpResponseBase
} from '@angular/common/http';
import Swal from 'sweetalert2';

@Injectable()
export class HandlerErrorInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    public toastr: ToastrService,
    private emitter: EmitterService,
  ) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.emitter.loading();
    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            if (event.status === 200 || event.status === 201) {
              if (event.body && this.emitter.isAllowToLoad) {
                if (event.body.message && event.body.message.trim() !== '') {
                  this.toast(
                    'success',
                    'Opération réussie',
                    'success'
                  );
                }
              }
            }
            this.emitter.stopLoading();
          }
        },
        (response: HttpResponseBase) => {
          this.emitter.stopLoading();
          this.emitter.allowLoading();
          if (response instanceof HttpErrorResponse) {
            switch (response.status) {
              case 200:
              case 201: {
                if (response.message.trim() !== '') {
                  this.toast(
                    response.message,
                    'Opération réussie',
                    'success'
                  );
                }
                break;
              }
              case 401: {
                this.toast(
                  'Vous avez été déconnecté !',
                  'Connexion requise',
                  'warning'
                );
                break;
              }
              case 402: {
                const errors = response.error.errors;
                if (!Array.isArray(errors)) {
                  Object.keys(errors).map(key => {
                    Swal.fire({
                      title: 'ACCES REFUSE !',
                      text: errors.msg,
                      icon: 'warning',
                      showConfirmButton: false,
                      cancelButtonText: 'Annuler',
                      allowOutsideClick: false,
                      hideClass: {
                        popup: 'swal2-hide',
                        backdrop: 'swal2-backdrop-hide',
                        icon: 'swal2-icon-hide'
                      }
                    })
                  });
                }
                break;
              }
              case 403: {
                this.toast(
                  'Vous n\'êtes pas autorisé a accéder a cette ressource !',
                  'Accès Réfusé',
                  'warning'
                );
                break;
              }
              case 404: {
                let msg = 'Vous n\'êtes pas autorisé a accéder a cette ressource !';
                if (response.hasOwnProperty('statusText')) {
                  msg = response.statusText;
                }
                this.toast(msg, 'Ressource introuvable', 'warning');
                break;
              }
              case 422: {
                const errors = response.error.errors;
                if (!Array.isArray(errors)) {
                  Object.keys(errors).map(key => {
                    this.toast(errors.msg, 'Contenu requis ou absent.', 'warning');
                  });
                }
                break;
              }
              case 500: {
                const errors = response.error.errors;
                if (errors && !Array.isArray(errors)) {
                  Object.keys(errors).map(key => {
                    this.toast(errors[key], 'Erreur', 'error');
                  });
                } else {
                  this.toast(
                    'Erreur survenue au niveau du serveur.',
                    'Erreur serveur.',
                    'error'
                  );
                }
                break;
              }
              default: {
                this.toast(
                  'Erreur survenue au niveau du serveur.',
                  'Erreur serveur.',
                  'error'
                );
              }
            }
          }
        }, () => {
          this.emitter.stopLoading();
        })
    );
  }

  toast(msg, title, type): void {
    if (type === 'info') {
      this.toastr.info(msg, title);
    } else if (type === 'success') {
      this.toastr.success(msg, title);
    } else if (type === 'warning') {
      this.toastr.warning(msg, title);
    } else if (type === 'error') {
      this.toastr.error(msg, title);
    }
  }
}
