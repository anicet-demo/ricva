import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { EmpotageModule } from './operations/empotage/empotage.module';
import { ExecutionModule } from './operations/execution/execution.module';


@NgModule({
  declarations: [
    PagesComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    EmpotageModule,
    ExecutionModule,
  ]
})
export class PagesModule { }
