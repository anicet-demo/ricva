import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'empotage',
    loadChildren: () => import('./operations/empotage/empotage.module').then(module => module.EmpotageModule)
  },
  { path: 'execution',
    loadChildren: () => import('./operations/execution/execution.module').then(module => module.ExecutionModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
