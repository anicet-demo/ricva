import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpotageRoutingModule } from './empotage-routing.module';
import { PlanEmpotageAddComponent } from './plan-empotage/plan-empotage-add/plan-empotage-add.component';
import { PlanEmpotageListComponent } from './plan-empotage/plan-empotage-list/plan-empotage-list.component';
import { PlanEmpotageShowComponent } from './plan-empotage/plan-empotage-show/plan-empotage-show.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatStepperModule} from '@angular/material/stepper';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    PlanEmpotageAddComponent,
    PlanEmpotageListComponent,
    PlanEmpotageShowComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    EmpotageRoutingModule,
    MatStepperModule,
    SharedModule,
  ]
})
export class EmpotageModule { }
