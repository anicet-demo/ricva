import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookingService } from 'src/app/services/booking.service';
import { ConteneurService } from 'src/app/services/conteneur.service';
import { ContratService } from 'src/app/services/contrat.service';
import { EmpotageService } from 'src/app/services/empotage.service';
import { EntrepotService } from 'src/app/services/entrepot.service';
import { LotService } from 'src/app/services/lot.service';
import { TransitaireService } from 'src/app/services/transitaire.service';
import { PlanEmpotageAddComponent } from './plan-empotage/plan-empotage-add/plan-empotage-add.component';
import { PlanEmpotageListComponent } from './plan-empotage/plan-empotage-list/plan-empotage-list.component';
import { PlanEmpotageShowComponent } from './plan-empotage/plan-empotage-show/plan-empotage-show.component';

const routes: Routes = [
  {
    path: "add",
    component: PlanEmpotageAddComponent,
    resolve: {
      contrats: ContratService,
      conteneurs: ConteneurService,
      lots: LotService,
      entrepots: EntrepotService,
      bookings: BookingService,
      transitaires: TransitaireService,
    }
  },
  {
    path: "list",
    component: PlanEmpotageListComponent,
    resolve: {empotages: EmpotageService}
  },
  {
    path: "single/:id",
    component: PlanEmpotageShowComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpotageRoutingModule { }
