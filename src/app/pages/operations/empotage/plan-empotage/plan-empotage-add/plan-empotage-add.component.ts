import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Booking } from 'src/app/models/booking';
import { Conteneur } from 'src/app/models/conteneur';
import { Contrat } from 'src/app/models/contrat';
import { Entrepot } from 'src/app/models/entrepot';
import { Lot } from 'src/app/models/lot';
import { Transitaire } from 'src/app/models/transitaire';
import { BookingService } from 'src/app/services/booking.service';
import { ConteneurService } from 'src/app/services/conteneur.service';
import { ContratService } from 'src/app/services/contrat.service';
import { EmpotageService } from 'src/app/services/empotage.service';
import { EntrepotService } from 'src/app/services/entrepot.service';
import { LotService } from 'src/app/services/lot.service';
import { TransitaireService } from 'src/app/services/transitaire.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-plan-empotage-add',
  templateUrl: './plan-empotage-add.component.html',
  styleUrls: ['./plan-empotage-add.component.scss']
})
export class PlanEmpotageAddComponent implements OnInit {
  conteneurs: Conteneur[]
  transitaires: Transitaire[]
  entrepots: Entrepot[]
  contrats: Contrat[]
  bookings: Booking[]
  conteneur: Conteneur
  transitaire: Transitaire
  entrepot: Entrepot
  contrat: Contrat
  booking: Booking
  form: FormGroup
  isLinear = false;
  detail = false;
  lots: Lot[]
  lot: Lot
  lotD: Lot

  constructor(
    private transitaireService: TransitaireService,
    private conteneurService: ConteneurService,
    private entrepotService: EntrepotService,
    private empotageService: EmpotageService,
    private bookingService: BookingService,
    private contratService: ContratService,
    public activatedRoute: ActivatedRoute,
    private lotService: LotService,
    private formBuild: FormBuilder
  ) {
    this.newForm()
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: {transitaires: Transitaire[]}) => { this.transitaires = data.transitaires })
    this.activatedRoute.data.subscribe((data: {conteneurs: Conteneur[]}) => { this.conteneurs = data.conteneurs })
    this.activatedRoute.data.subscribe((data: {entrepots: Entrepot[]}) => { this.entrepots = data.entrepots })
    this.activatedRoute.data.subscribe((data: {bookings: Booking[]}) => { this.bookings = data.bookings })
    this.activatedRoute.data.subscribe((data: {contrats: Contrat[]}) => { this.contrats = data.contrats })
    this.activatedRoute.data.subscribe((data: {lots: Lot[]}) => { this.lots = data.lots })
  }

  newForm(){
    this.form = this.formBuild.group({
      id: [null],
      contrat: [null],
      etat: [true],
      qte: [0],
      premium: [0],
      discompte: [0],
      booking: [null],
      conteneur: [null],
      entrepot: [null],
      transitaire: [null],
      lot: [null],
      create: [null],
      update: [null],
    })
  }
  showDetail(row: Lot) {
    this.lot = row
    this.detail = !this.detail;
  }
  onSetData(){
    if (this.transitaire) {
      this.transitaireService.getSingle(this.f.transitaire.value).subscribe(res =>{ return this.transitaire = res })
    }
    if (this.conteneur) {
      this.conteneurService.getSingle(this.f.conteneur.value).subscribe(res =>{ return this.conteneur = res })
    }
    if (this.conteneur) {
      this.conteneurService.getSingle(this.f.conteneur.value).subscribe(res =>{ return this.conteneur = res })
    }
    if (this.entrepot) {
      this.entrepotService.getSingle(this.f.entrepot.value).subscribe(res =>{ return this.entrepot = res })
    }
    if (this.booking) {
      this.bookingService.getSingle(this.f.booking.value).subscribe(res =>{ return this.booking = res })
    }
    if (this.contrat) {
      this.contratService.getSingle(this.f.contrat.value).subscribe(res =>{ return this.contrat = res })
    }
    if (this.lot) {
      this.lotService.getSingle(this.f.lot.value).subscribe(res =>{ return this.lotD = res })
    }
  }
  onSubmit(){
    this.f.create.setValue(new Date())
    const data = this.form.getRawValue();
    Swal.fire({
      title:  "<h5 style='color:#CB7A4B; font-size: 2rem; font-weight: bold; margin-top:30px'>" + 'Voulez-vous enregistre' + "</h5>",
      text: " Êtes-vous sure?",
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonColor: '#f5d6c8',
      cancelButtonColor: '#CB7A4B',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then((result) => {
      if (result.isConfirmed) {
        this.empotageService.add(data).subscribe(res => { });
      }
    })
  }
  get f() { return this.form.controls }
}
