import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanEmpotageAddComponent } from './plan-empotage-add.component';

describe('PlanEmpotageAddComponent', () => {
  let component: PlanEmpotageAddComponent;
  let fixture: ComponentFixture<PlanEmpotageAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanEmpotageAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanEmpotageAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
