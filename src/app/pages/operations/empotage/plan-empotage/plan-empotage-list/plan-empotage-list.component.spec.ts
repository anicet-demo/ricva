import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanEmpotageListComponent } from './plan-empotage-list.component';

describe('PlanEmpotageListComponent', () => {
  let component: PlanEmpotageListComponent;
  let fixture: ComponentFixture<PlanEmpotageListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanEmpotageListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanEmpotageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
