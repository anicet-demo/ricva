import { EmpotageService } from './../../../../../services/empotage.service';
import { Empotage } from './../../../../../models/empotage';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-plan-empotage-list',
  templateUrl: './plan-empotage-list.component.html',
  styleUrls: ['./plan-empotage-list.component.scss']
})
export class PlanEmpotageListComponent implements OnInit {
  empotages: Empotage[] = []

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private empotageService: EmpotageService
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {empotages: Empotage[]}) => {
      this.empotages = data.empotages
    })
  }
  addEmpotage(){
    this.router.navigate(['/pages/empotage/add'])
  }
  empotageShow(row: Empotage){
    this.router.navigate(['/pages/empotage/single/'+row.id])
  }
  onDelete(row, i){
    Swal.fire({
      title:  "<h5 style='color:#CB7A4B; font-size: 2rem; font-weight: bold; margin-top:30px'>" + 'Voulez-vous supprimer' + "</h5>",
      text: " Vous ne pourrez pas revenir en arrière !",
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonColor: '#f5d6c8',
      cancelButtonColor: '#CB7A4B',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then((result) => {
      if (result.isConfirmed) {
        this.empotageService.getDelete(row.id).subscribe(res => {
          this.empotages.splice(0, i)
        }, error => {})
      }
    })
  }
}
