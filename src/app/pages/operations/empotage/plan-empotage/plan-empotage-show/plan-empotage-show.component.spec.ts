import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanEmpotageShowComponent } from './plan-empotage-show.component';

describe('PlanEmpotageShowComponent', () => {
  let component: PlanEmpotageShowComponent;
  let fixture: ComponentFixture<PlanEmpotageShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanEmpotageShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanEmpotageShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
