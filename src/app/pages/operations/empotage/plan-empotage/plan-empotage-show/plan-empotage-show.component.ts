import { Lot } from 'src/app/models/lot';
import { ActivatedRoute } from '@angular/router';
import { Booking } from 'src/app/models/booking';
import { Contrat } from 'src/app/models/contrat';
import { Component, OnInit } from '@angular/core';
import { Empotage } from 'src/app/models/empotage';
import { Entrepot } from 'src/app/models/entrepot';
import { Conteneur } from 'src/app/models/conteneur';
import { Transitaire } from 'src/app/models/transitaire';
import { LotService } from 'src/app/services/lot.service';
import { ContratService } from 'src/app/services/contrat.service';
import { BookingService } from 'src/app/services/booking.service';
import { EmpotageService } from 'src/app/services/empotage.service';
import { EntrepotService } from 'src/app/services/entrepot.service';
import { ConteneurService } from 'src/app/services/conteneur.service';
import { TransitaireService } from 'src/app/services/transitaire.service';

@Component({
  selector: 'app-plan-empotage-show',
  templateUrl: './plan-empotage-show.component.html',
  styleUrls: ['./plan-empotage-show.component.scss']
})
export class PlanEmpotageShowComponent implements OnInit {
  action ='non valide';
  lot: Lot;
  contrat: Contrat;
  booking: Booking;
  empotage: Empotage;
  entrepot: Entrepot;
  conteneur: Conteneur;
  transitaire: Transitaire;
  etat: boolean = false

  constructor(
    private route: ActivatedRoute,
    private lotService: LotService,
    private contratService: ContratService,
    private bookingService: BookingService,
    private empotageService: EmpotageService,
    private entrepotService: EntrepotService,
    private conteneurService: ConteneurService,
    private transitaireService: TransitaireService
  ) { }

  ngOnInit(): void {
    this.empotageService.getSingle(this.route.snapshot.params.id).subscribe((res: any) => {
      this.setData(res)
      return this.empotage = res
    });
  }
  setData(empotage){
    if (empotage) {
      if (empotage?.lot) {
        this.lotService.getSingle(empotage?.lot).subscribe((res: any) => { return this.lot = res });
      }
      if (empotage?.contrat) {
        this.contratService.getSingle(empotage?.contrat).subscribe((res: any) => { return this.contrat = res });
      }
      if (empotage?.booking) {
        this.bookingService.getSingle(empotage?.booking).subscribe((res: any) => { return this.booking = res });
      }
      if (empotage?.entrepot) {
        this.entrepotService.getSingle(empotage?.entrepot).subscribe((res: any) => { return this.entrepot = res });
      }
      if (empotage?.conteneur) {
        this.conteneurService.getSingle(empotage?.conteneur).subscribe((res: any) => { return this.conteneur = res });
      }
      if (empotage?.transitaire) {
        this.transitaireService.getSingle(empotage?.transitaire).subscribe((res: any) => { return this.transitaire = res });
      }
    }

  }
  clicked(el:string) {
    this.etat = this.etat ? false : true
  }

}
