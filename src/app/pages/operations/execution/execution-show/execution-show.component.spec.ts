import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionShowComponent } from './execution-show.component';

describe('ExecutionShowComponent', () => {
  let component: ExecutionShowComponent;
  let fixture: ComponentFixture<ExecutionShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExecutionShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
