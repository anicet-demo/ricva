import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExecutionRoutingModule } from './execution-routing.module';
import { ExecutionAddComponent } from './execution-add/execution-add.component';
import { ExecutionListComponent } from './execution-list/execution-list.component';
import { ExecutionShowComponent } from './execution-show/execution-show.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ExecutionAddComponent,
    ExecutionListComponent,
    ExecutionShowComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    MatStepperModule,
    ExecutionRoutingModule
  ]
})
export class ExecutionModule { }
