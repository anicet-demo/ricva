import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Execute } from 'src/app/models/execute';
import { ExecuteService } from 'src/app/services/execute.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-execution-list',
  templateUrl: './execution-list.component.html',
  styleUrls: ['./execution-list.component.scss']
})
export class ExecutionListComponent implements OnInit {
  executes: Execute[] = []

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private executeService: ExecuteService
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {executes: Execute[]}) => {
      this.executes = data.executes
    })
  }
  addExecution(){
    this.router.navigate(['/pages/execution/add'])
  }
}
