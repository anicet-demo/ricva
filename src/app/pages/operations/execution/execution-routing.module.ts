import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConteneurService } from 'src/app/services/conteneur.service';
import { EmpotageService } from 'src/app/services/empotage.service';
import { ExecuteService } from 'src/app/services/execute.service';
import { LotService } from 'src/app/services/lot.service';
import { ExecutionAddComponent } from './execution-add/execution-add.component';
import { ExecutionListComponent } from './execution-list/execution-list.component';
import { ExecutionShowComponent } from './execution-show/execution-show.component';

const routes: Routes = [
  {
    path: "add",
    component: ExecutionAddComponent,
    resolve: {conteneurs: ConteneurService, empotages: EmpotageService, lots: LotService, }
  },
  {
    path: "list",
    component: ExecutionListComponent,
    resolve: {executes: ExecuteService}
  },
  {
    path: "single/:id",
    component: ExecutionShowComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutionRoutingModule { }
