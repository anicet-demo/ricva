import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionAddComponent } from './execution-add.component';

describe('ExecutionAddComponent', () => {
  let component: ExecutionAddComponent;
  let fixture: ComponentFixture<ExecutionAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExecutionAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
