import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Conteneur } from 'src/app/models/conteneur';
import { Empotage } from 'src/app/models/empotage';
import { Lot } from 'src/app/models/lot';
import { ConteneurService } from 'src/app/services/conteneur.service';
import { EmitterService } from 'src/app/services/emitter.service';
import { EmpotageService } from 'src/app/services/empotage.service';
import { ExecuteService } from 'src/app/services/execute.service';
import { LotService } from 'src/app/services/lot.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-execution-add',
  templateUrl: './execution-add.component.html',
  styleUrls: ['./execution-add.component.scss']
})
export class ExecutionAddComponent implements OnInit {
  isLinear = false;
  edit = false;
  firstFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;
  form: FormGroup
  conteneurs: Conteneur[]
  empotages: Empotage[]
  lots: Lot[]

  constructor(
    public router: Router,
    public toastr: ToastrService,
    private formBuild: FormBuilder,
    private emitter: EmitterService,
    public activatedRoute: ActivatedRoute,
    private executeService: ExecuteService,
  ) {
    this.newForm()
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: {conteneurs: Conteneur[]}) => { this.conteneurs = data.conteneurs })
    this.activatedRoute.data.subscribe((data: {empotages: Empotage[]}) => { this.empotages = data.empotages })
    this.activatedRoute.data.subscribe((data: {lots: Lot[]}) => { this.lots = data.lots })
  }
  newForm(){
    this.form = this.formBuild.group({
      id: [null],
      etat: [true],
      empotage: [null],
      create: [null],
      update: [null],
      conteneurs: this.formBuild.array([
        this.formBuild.group({
          id: [null],
          conteneur: [null],
          lots: this.formBuild.array([
            this.formBuild.group({
              id: [null],
              lot: [null, [Validators.required]],
              nbSacs: [null]
            })
          ]),
        })
      ]),
    })
  }
  onAddConteneur() {
    return this.optCont.push(
      this.formBuild.group({
        uuid: [null],
        id: [null],
        conteneur: [null],
        lots: this.formBuild.array([
          this.formBuild.group({
            id: [null],
            lot: [null],
            nbSacs: [null]
          })
        ]),
      })
    );
  }
  onAddLot(cont, count, item) {
    console.log(item.get('lots').controls)
  }
  onSubmit(){
    this.f.create.setValue(new Date())
    const data = this.form.getRawValue();
    Swal.fire({
      title:  "<h5 style='color:#CB7A4B; font-size: 2rem; font-weight: bold; margin-top:30px'>" + 'Voulez-vous enregistre' + "</h5>",
      text: " Êtes-vous sure?",
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonColor: '#f5d6c8',
      cancelButtonColor: '#CB7A4B',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then((result) => {
      if (result.isConfirmed) {
        this.executeService.add(data).subscribe(res => {
          this.router.navigate(['pages/execution/list'])
        });
      }
    })
  }
  toast(msg, title, type): void {
    if (type === 'info') {
      this.toastr.info(msg, title);
    } else if (type === 'success') {
      this.toastr.success(msg, title);
    } else if (type === 'warning') {
      this.toastr.warning(msg, title);
    } else if (type === 'error') {
      this.toastr.error(msg, title);
    }
  }
  get f() { return this.form.controls }
  get optCont() { return (this.form.get('conteneurs') as FormArray); }
}
